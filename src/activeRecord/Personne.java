package activeRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * class Personne possedant 3 attributs id, nom, prenom
 */
public class Personne {

	private int id;
	private String nom;
	private String prenom;
	
	
/**
 * constructeur de la classe personne qui construit une Personne à partir de son nom et de son prénom et initialise l'id à -1
 * @param n qui instancie le nom d'une personne
 * @param p qui instancie le prénom d'une personne
 */
	public Personne(String n, String p){
		nom=n;
		prenom=p;
		id=-1;
	}
	/**
	 * constructeur de la classe personne qui construit une Personne à partir de son nom et de son prénom et de son id 
	 * @param n qui instancie le nom d'une personne
	 * @param p qui instancie le prénom d'une personne
	 * @param i qui initialise id d'une personne
	 */
	private Personne(int i, String n, String p){
		id=i;
		nom=n;
		prenom=p;
	}
/**
 * méthode chargée de retourner l'ensemble des tuples de la classe Personne sous la forme d'objets.
 * @return res qui est une ArrayList de Personne
 * @throws SQLException
 */
	public static ArrayList<Personne> findAll() throws SQLException{
		String SQLPrep = "SELECT * FROM Personne;";
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement(SQLPrep);
		ArrayList<Personne> res=new ArrayList<Personne>();
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		while (rs.next()) {
			String n = rs.getString("nom");
			String p = rs.getString("prenom");
			int i = rs.getInt("id");
			res.add(new Personne(i,n,p));
		}
		return res;
	}
/**
 * methode qui retourne l'objet Personne correspondant au tuple avec l'id passé en paramètre (null si l'objet n'existe pas)
 * @param i correspondant à l'id de la personne recherchée
 * @return p qui est la Personne recherchée
 * @throws SQLException
 */
	public static Personne findById(int i) throws SQLException{
		String SQLPrep = "SELECT * FROM Personne where id=?;";
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement(SQLPrep);
		prep1.setInt(1, i);
		Personne p=null;
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		while (rs.next()) {
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");
			int id = rs.getInt("id");
			p= new Personne(id,nom,prenom);
		}
		return p;
	}

	/**
	 * méthode qui retourne la liste des objets Personne correspondant aux tuples dont le nom est passé en paramètre
	 * @param s correspondant au nom de la personne recherchée
	 * @return res qui correspond à une ArrayList de personnes possédant ce nom
	 * @throws SQLException
	 */
	public static ArrayList<Personne> findByName(String s) throws SQLException{
		String SQLPrep = "SELECT * FROM Personne where nom = ?;";
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement(SQLPrep);
		prep1.setString(1, s);
		ArrayList<Personne> res=new ArrayList<Personne>();
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		while (rs.next()) {
			String n = rs.getString("nom");
			String p = rs.getString("prenom");
			int i = rs.getInt("id");
			res.add(new Personne(i,n,p));
		}
		return res;
	}
	
	
/**
 * méthode qui permet de créer la table Personne de la base de données testPersonne
 * @throws SQLException
 */
	public static void createTable() throws SQLException{
		String createString = "CREATE TABLE Personne ( " + "ID INTEGER  AUTO_INCREMENT, "
				+ "NOM varchar(40) NOT NULL, " + "PRENOM varchar(40) NOT NULL, " + "PRIMARY KEY (ID))";
		Statement stmt = DBConnection.getConnection().createStatement();
		stmt.executeUpdate(createString);
	}
/**
 * méthode qui permet de supprimer la table Personne de la base de données testPersonne
 * @throws SQLException
 */
	public static void deleteTable() throws SQLException{
		String drop = "DROP TABLE Personne";
		Statement stmt = DBConnection.getConnection().createStatement();
		stmt.executeUpdate(drop);
	}
	
	/**
	 * méthode qui permet de supprimer une Personne de la table personne
	 * @throws SQLException
	 */
	public void delete() throws SQLException{
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement("delete from personne where id=?;");
		prep1.setInt(1, this.id);
		prep1.executeUpdate();
		this.setId(-1);
	}
/**
 * retourne le nom de la Personne
 * @return nom
 */
	public String getNom() {
		return nom;
	}
/**
 * met à jour le nom de la personne 
 * @param nom
 */
	public void setNom(String nom) {
		this.nom = nom;
	}
/**
 * retourne le prenom de la personne
 * @return prenom
 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * met à jour le prenom
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * retourne l'id de la personne
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * sauve l'objet Personne dans la table
	 * @throws SQLException
	 */
	public void save() throws SQLException {
		if(id==-1){
			this.saveNew();
		}
		else {
			this.update();
		}
	}
	
	/**
	 * insère l'objet dans la table personne et met à jour l'id de l'objet avec l'indice crée par la table grâce à l'auto-increment
	 * @throws SQLException
	 */
	private void saveNew() throws SQLException{
		String SQLPrep = "INSERT INTO Personne (nom, prenom) VALUES (?,?);";
		PreparedStatement prep = DBConnection.getConnection().prepareStatement(SQLPrep, Statement.RETURN_GENERATED_KEYS);
		prep.setString(1, nom);
		prep.setString(2, prenom);
		prep.executeUpdate();
		Statement stmt=DBConnection.getConnection().createStatement();
		ResultSet rs =stmt.executeQuery("select count(*) from personne;");
		while(rs.next()) this.setId(rs.getInt(1));
	}
	
	/**
	 * met à jour le tuple existant
	 * @throws SQLException
	 */
	private void update() throws SQLException{
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement("update personne set nom=? , prenom=? where id=? ;");
		prep1.setInt(3, this.id);
		prep1.setString(2, this.getPrenom());
		prep1.setString(1, this.getNom());
		prep1.executeUpdate();
	}

	/**
	 * met à jour l'id de la personne
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personne other = (Personne) obj;
		if (id != other.id)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}

	public static void main(String[] args) throws SQLException{
		Personne.deleteTable();
	}
	
	


}
