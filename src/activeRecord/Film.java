package activeRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 * class film
 * @author samy et Axel
 *
 */
public class Film {
	private String titre;
	private int id, id_real;
	
	/**
	 * construit un Film à partir de la chaîne correspondant à son titre et à partir de l'objet Personne correspondant à son réalisateur 
	 * @param t titre du film
	 * @param p réalisateur de type Personne
	 */
	public Film(String t, Personne p){
		titre=t;
		id_real=p.getId();
		id=-1;
	}
	/**
	 * constructeur privé qui construit un Film en prenant en paramètre des ids et un titre
	 * @param idf identifiant du film
	 * @param t titre du film
	 * @param idr identifiant du réalisateur
	 */
	private Film(int idf, String t, int idr){
		titre=t;
		id_real=idr;
		id=idf;
	}
	
	/**
	 * méthode qui permet de créer la table Film de la base de données testPersonne
	 * @throws SQLException
	 */
	public static void createTable() throws SQLException{
		String createString = "CREATE TABLE IF NOT EXISTS `film` ("
				+ "`ID` int(11) NOT NULL AUTO_INCREMENT,"
				+ "`TITRE` varchar(40) NOT NULL,"
				+ "`ID_REA` int(11) DEFAULT NULL,"
				+ "PRIMARY KEY (`ID`),"
				+ "KEY `ID_REA` (`ID_REA`)"
				+ ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;";
		Statement stmt = DBConnection.getConnection().createStatement();
		stmt.executeUpdate(createString);
		Statement stmt1 = DBConnection.getConnection().createStatement();
		stmt1.executeUpdate("ALTER TABLE `film` ADD CONSTRAINT `film_ibfk_1` FOREIGN KEY (`ID_REA`) REFERENCES `personne` (`ID`);");
	}
	
	/**
	 * méthode qui permet de supprimer la table Film de la base de données testPersonne
	 * @throws SQLException
	 */
	public static void deleteTable() throws SQLException{
		String drop = "DROP TABLE Film";
		Statement stmt = DBConnection.getConnection().createStatement();
		stmt.executeUpdate(drop);
	}
	
	/**
	 * retourne l'objet Film correspondant au tuple avec l'id passé en paramètre
	 * @param i correspond à l'id du film recherché
	 * @return f correspondant au film recherché
	 * @throws SQLException
	 */
	public static Film findById(int i) throws SQLException{
		String SQLPrep = "SELECT * FROM Film where id=?;";
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement(SQLPrep);
		prep1.setInt(1, i);
		Film f=null;
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		while (rs.next()) {
			int rea = rs.getInt("id_rea");
			int idf = rs.getInt("id");
			String t = rs.getString("titre");
			f= new Film(idf,t,rea);
		}
		return f;
	}
	
	/**
	 * retourne le réalisateur
	 * @return personne
	 * @throws SQLException
	 */
	public Personne getRealisateur() throws SQLException{
		return Personne.findById(this.id_real);
	}
	
	/**
	 * retourne le titre du film
	 * @return titre
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * met à jour le titre du film
	 * @param titre
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}

	/**
	 * retourne l'id du film
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * met à jour l'id du film
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * retourne l'id du réalisateur
	 * @return id_real
	 */
	public int getId_real() {
		return id_real;
	}

	/**
	 * met à jour l'idée du réalisateur
	 * @param id_real
	 */
	public void setId_real(int id_real) {
		this.id_real = id_real;
	}
	
	/**
	 * sauve l'objet dans la table film
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	public void save() throws SQLException, RealisateurAbsentException {
		if(id_real==-1)
			throw new RealisateurAbsentException();
		if(id==-1){
			this.saveNew();
		}
		else {
			this.update();
		}

	}
	
	/**
	 * insère l'objet dans la table film
	 * @throws SQLException
	 */
	private void saveNew() throws SQLException{
		String SQLPrep = "INSERT INTO film (titre, id_rea) VALUES (?,?);";
		PreparedStatement prep = DBConnection.getConnection().prepareStatement(SQLPrep, Statement.RETURN_GENERATED_KEYS);
		prep.setString(1, titre);
		prep.setInt(2, id_real);
		prep.executeUpdate();
		Statement stmt=DBConnection.getConnection().createStatement();
		ResultSet rs =stmt.executeQuery("select count(*) from film;");
		while(rs.next()) this.setId(rs.getInt(1));
	}
	
	/**
	 * met à jour le tuple existant 
	 * @throws SQLException
	 */
	private void update() throws SQLException{
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement("update film set titre=? , id_rea=? where id=? ;");
		prep1.setInt(3, this.id);
		prep1.setInt(2, id_real);
		prep1.setString(1, titre);
		prep1.executeUpdate();
	}
	
	/**
	 * retourne l'ensemble des films réalisé par la personne passée en paramètre.
	 * @param p correspond au réalisateur recherché
	 * @return res correspondant à une ArrayList de Film
	 * @throws SQLException
	 */
	public static ArrayList<Film> findByRealisateur(Personne p) throws SQLException{
		ArrayList<Film> res= new ArrayList<Film>();
		String SQLPrep = "SELECT * FROM Film where id_rea=?;";
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement(SQLPrep);
		prep1.setInt(1, p.getId());
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		while (rs.next()) {
			int real = rs.getInt("id_rea");
			int idf = rs.getInt("id");
			String t = rs.getString("titre");
			res.add(new Film(idf,t,real));
		}
		return res;
	}
	
	@Override
	public String toString() {
		return "Film [titre=" + titre + ", id=" + id + ", id_real=" + id_real + "]";
	}
	
	/**
	 * permet de supprimer un film de la table Film
	 * @throws SQLException
	 */
	public void delete() throws SQLException{
		PreparedStatement prep1 = DBConnection.getConnection().prepareStatement("delete from film where id=?;");
		prep1.setInt(1, this.id);
		prep1.executeUpdate();
		this.setId(-1);
	}
	
	/**
	 * méthode main
	 * @param args
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	public static void main(String[] args) throws SQLException, RealisateurAbsentException {
		Personne.createTable();
		new Personne("axel","froehlicher").save();
		Film.createTable();
		new Film("test", Personne.findById(1)).save();
		Film f=new Film("test2", Personne.findById(1));
		f.save();
		Personne p= Personne.findById(1);
		System.out.println(p);
		f.delete();
		ArrayList<Film> res=Film.findByRealisateur(p);
		System.out.println(res);
		Film.deleteTable();
		Personne.deleteTable();
		
	}
	
}
