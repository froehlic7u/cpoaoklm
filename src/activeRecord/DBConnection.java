package activeRecord;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.mysql.jdbc.Connection;

public class DBConnection {

	private static Connection connect;
	private static String dbName = "testpersonne";

	private DBConnection() throws SQLException{
		
		// variables a modifier en fonction de la base
		String userName = "root";
		String password = "";
		String serverName = "localhost";
		//Attention, sous MAMP, le port est 8889
		String portNumber = "3306";
		String tableName = "personne";

		// creation de la connection
		Properties connectionProps = new Properties();
		
		connectionProps.put("user", userName);
		connectionProps.put("password", password);
		String urlDB = "jdbc:mysql://" + serverName + ":";
		urlDB += portNumber + "/" + dbName;
		connect = (Connection) DriverManager.getConnection(urlDB, connectionProps);
	}
	
	/**
	 * méthode qui permet de récuperer une connexion
	 * @return connect qui correspond à une connexion
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException{
		if(connect == null)
			new DBConnection();
		return connect;
	}
	
	/**
	 * permet de changer le nom de la base demandée
	 * @param nomDB
	 * @throws SQLException
	 */
	public static void setNomDB(String nomDB) throws SQLException{
		dbName=nomDB;
		new DBConnection();
	}


}
