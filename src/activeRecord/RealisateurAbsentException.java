package activeRecord;

public class RealisateurAbsentException extends Exception {
	
	/*
	 * Constructeur de RealisateurAbsentException
	 */
	public RealisateurAbsentException(){
		super("Realisateur absent dans la base de donn�e");
	}
}
