package testEtu;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import activeRecord.Personne;
/**
 * class de test pour la class Personne
 * @author samy et Axel
 *
 */
public class testPersonne {

	/**
	 * initialisation
	 * @throws SQLException
	 */
	@Before
	public void initialiser() throws SQLException {
		Personne.createTable();
		Personne p1=new Personne("froehlicher","axel");
		Personne p2=new Personne("berger","pierre");
		p1.save();
		p2.save();
	}
	
	/**
	 * test de la méthode findByID() qui vérifie si la méthode fonctionne pour un ID existant
	 * @throws SQLException
	 */
	@Test
	public void findByIDExistant_test() throws SQLException{
		Personne p1= Personne.findById(1);
		Personne p2= new Personne("froehlicher","axel");
		p2.setId(1);
		assertTrue(p1.equals(p2));
	}
	
	/**
	 * verifie que pour un id inexistant, la personne est bien null
	 * @throws SQLException
	 */
	@Test
	public void findByIDInexistant_test() throws SQLException{
		Personne p= Personne.findById(4);
		assertTrue(p==null);
	}
	
	/**
	 * test de la méthode findByIDALL()
	 * @throws SQLException
	 */
	@Test
	public void findByIDAll_test() throws SQLException{
		assertTrue(2==Personne.findAll().size());
	}
	
	/**
	 * test de la méthode findByName pour une personne présente
	 * @throws SQLException
	 */
	@Test
	public void findByIDByNameExistant_test() throws SQLException{
		ArrayList<Personne> p1= Personne.findByName("froehlicher");
		Personne p2= new Personne("froehlicher","axel");
		p2.setId(1);
		assertTrue(p1.contains(p2));
	}	
	/**
	 * test de la méthode findByName pour une personne inexistante
	 * @throws SQLException
	 */
	@Test
	public void findByIDByNameInexistant_test() throws SQLException{
		ArrayList<Personne> p1= Personne.findByName("george");
		assertTrue(p1.size()==0);
	}	
	
	/**
	 * test de la méthode deleteTable pour supprimer une table
	 * @throws SQLException
	 */
	@After
	public void supprimer() throws SQLException {
		Personne.deleteTable();
	}
	

}
