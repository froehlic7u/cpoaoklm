package testEtu;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

import activeRecord.DBConnection;

public class TestConnextion {
	
	/**
	 * test de la méthode getConnection pour qu'une connection est unique
	 * @throws SQLException
	 */
	@Test
	public void connectionIdentique() throws SQLException{
		assertEquals("la connection doit �tre identiques", DBConnection.getConnection(),DBConnection.getConnection());
	}
	
	/**
	 * test qui vérifie que la connexion est différente
	 * @throws SQLException
	 */
	@Test
	public void connectionDifferent() throws SQLException{
		Connection c1=DBConnection.getConnection();
		DBConnection.setNomDB("test");
		Connection c2=DBConnection.getConnection();
		assertFalse("la connection doit �tre differents", c1==c2);
	}

}
