package testEtu;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import activeRecord.Film;
import activeRecord.Personne;
import activeRecord.RealisateurAbsentException;

public class TestFilm {

	private Personne p1, p2, p3;
	
	/**
	 * Initialisation des test
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	@Before
	public void initialisation() throws SQLException, RealisateurAbsentException{
		Personne.createTable();
		Film.createTable();
		p1=new Personne("nP1", "pP1");
		p2=new Personne("nP2", "pP2");
		p3=new Personne("nP3", "pP3");
		p1.save();
		p2.save();
		p3.save();
		
		new Film("les 4 fantastiques",p1).save();
		new Film("spider-man", p2).save();
		new Film("cartoon", p3).save();
	}
	
	/**
	 * test de la presence dans la base � la cr�ation du film
	 */
	@Test
	public void test_FilmHorsBD(){
		Film f=new Film("test", p1);
		assertEquals("Le film ne doit pas etre dans la bd", -1,f.getId());
	}
	
	/**
	 * test de la methode findById() avec un film present dans la base
	 * @throws SQLException
	 */
	@Test
	public void test_findByIdExistant() throws SQLException{
		Film f=Film.findById(1);
		assertEquals("le titre est mauvais","les 4 fantastiques", f.getTitre());
	}
	
	/**
	 * test de la methode findById() avec un film absent dans la base, le film devrait etre null
	 * @throws SQLException
	 */
	@Test
	public void test_findByiDInexistant() throws SQLException{
		Film f = Film.findById(5);
		assertEquals("le film devrait etre null", null, f);
	}
	
	/**
	 * test de la methode qui retourne la personne correspondant au realisateur
	 * @throws SQLException
	 */
	@Test
	public void test_getRea() throws SQLException{
		Film f = Film.findById(1);
		Personne p = f.getRealisateur();
		assertEquals("le realisateur n'est pas le bon", "nP1", p.getNom());
	}
	
	/**
	 * test du declanchement de l'exception RealisateurAbsent lorsque que l'on ajoute un realisateur absent dans la table persone
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	@Test (expected = activeRecord.RealisateurAbsentException.class)
	public void test_saveReaInexistant() throws RealisateurAbsentException, SQLException {
		new Film("tranquille", new Personne("axel","froehlicher")).save();
	}
	
	/**
	 * test de la methode qui retourne les film d'n realisateur
	 * @throws SQLException
	 */
	@Test
	public void test_findByRea() throws SQLException{
		ArrayList<Film> a = Film.findByRealisateur(p1);
		assertEquals("mauavais nombre de film", 1, a.size());
		assertEquals("mauvais film","les 4 fantastiques", a.get(0).getTitre());
	}
	
	/**
	 * test de la mise a jour d'un film
	 * @throws SQLException
	 */
	@Test
	public void test_saveUpdate() throws SQLException{
		assertEquals("mauvais nom", "nP2", p2.getNom());
		p2.setNom("test");
		p2.save();
		assertEquals("mauvais nom apres update", "test", p2.getNom());
	}
	
	/**
	 * test de la methode de suppression dans la table film
	 * @throws SQLException
	 */
	@Test
	public void test_delete() throws SQLException{
		assertEquals("mauavsi titre", "cartoon", Film.findById(3).getTitre());
		Film.findById(3).delete();
		assertEquals("mauvais film", null, Film.findById(3));
	}
	
	/**
	 * suppression des tables
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	@After
	public void delete() throws SQLException{
		Film.deleteTable();
		Personne.deleteTable();
	}
	
}
